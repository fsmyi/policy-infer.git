# 策略infer



#### 使用说明

1.  预测代码为my_infer.ipynb , .py文件为辅助文件
2.  预测数据及其他文件，分享到了百度云盘
链接：https://pan.baidu.com/s/1Bi5iEP0eSGPTTHYmPBcHjg 
提取码：5gfh
3.  需要GPU 
4.  其他问题，请加QQ群： 923858360(备注：量化)
5. 数据更新(22.6)，替换相应文件，就可以测试最新交易数据。
链接：https://pan.baidu.com/s/1RfCwWezOnj9Mxe1GCxW7CA 
提取码：hcv8

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
